
# Concept

## Qu'est ce que UML ?

![use_cases](img/logo-uml.png)

Crédit : https://fr.wikipedia.org/wiki/UML_(informatique)

Le **Langage de Modélisation Unifié**, de l'anglais _Unified Modeling Language_ (**UML**), est un langage de modélisation graphique à base de pictogrammes conçu pour fournir une méthode normalisée pour visualiser la conception d'un système. Il est couramment utilisé en développement logiciel et en conception orientée objet.

## Les diagrammes

### Diagramme de classes

Le  **diagramme de classes**  est un schéma utilisé en developpement informatique pour présenter les classes et les interfaces des systèmes ainsi que les différentes relations entre celles-ci. Il est généralement utilisé en POO (programation orientée objet) pour représenter toute les classes d'un système ainsi que leurs intéractions (héritage, interface, ...)
Exemple de diagramme de classe
![Exemple de diagramme de classe](img/diag_classe.png "Diagramme de classe")
#### Classe
Une classe est représentée par un rectangle séparé en trois parties :
-   la première partie contient le **nom** de la classe
-   la seconde contient les **attributs** de la classe
-   la dernière contient les **méthodes** de la classe

![Exemple de classe de diagramme de classe](img/diag_classe_classe.png "Classe de diagramme de classe")
#### Visibilité
La notion de visibilité indique qui peut avoir **accès** à l'attribut ou à la méthode.
Elle ne peut prendre que 4 valeurs possibles :

| Caractère | Mot clé |
|--|--|
| + | public |
| \# | protected |
| ~ | package |
| - | private |
#### Héritage
L'héritage est un principe de division par généralisation et spécialisation, représenté par un trait reliant les deux classes et dont l'extrémité du côté de la classe mère comporte un triangle.

La classe fille hérite de tous les attributs et méthodes, qu'ils soient publics, protégés ou privés. Cependant, elle ne peut pas utiliser directement les attributs et méthodes privés (que ce soit en lecture ou en écriture), sauf par l'intermédiaire d'une méthode héritée (publique ou protégée).

![Exemple d'héritage de diagramme de classe](./img/diag_classe_heritage.png "Héritage de diagramme de classe")

#### Crédit
[https://fr.wikipedia.org/wiki/UML_(informatique)](https://fr.wikipedia.org/wiki/UML_(informatique))

### Diagramme de déploiement

Le diagramme de déploiement décrit le déploiement physique des informations générées par le site web sur des composants matériels
Les diagrammes de déploiement sont constitués de plusieurs formes UML.
Les boîtes en trois dimensions appelées nœuds représentent les composants du système, qu'ils soient logiciels ou matériels.
Les lignes entre les nœuds indiquent les relations et les petites formes à l'intérieur des boîtes représentent les informations générées qui sont déployés.

![Noeud](img/diagDeploy.png)


Les diagrammes de déploiement sont utiles pour :

Montrer quels éléments logiciels sont déployés par quels éléments matériels.

Illustrer le traitement d'exécution du point de vue matériel.

Visualiser la topologie du système matériel.


Symboles et nomenclature des diagrammes de déploiement :

![Noeud](img/Node.jpeg)
![Base de donnée](img/database.jpeg)
-   **Artefact :**  produit développé par le logiciel, symbolisé par un rectangle avec le nom et le mot « artefact » entourés de flèches doubles.

-   **Association :**  ligne indiquant un message ou tout autre type de communication entre deux nœuds.

-   **Composant :**  rectangle avec deux onglets indiquant un élément logiciel.

-   **Dépendance** : ligne en pointillés terminée par une flèche, qui indique qu'un nœud ou composant est dépendant d'un autre.

-   **Interface** : cercle qui indique une relation contractuelle. Les objets qui réalisent l'interface doivent remplir une sorte d'obligation.

-   **Nœud :**  élément matériel ou logiciel représenté par une boîte en relief.

-   **Nœud** **conteneur :**  nœud qui en contient un autre, comme dans l'exemple ci-dessous où les nœuds contiennent des composants.

-   **Stéréotype :**  dispositif contenu dans le nœud, présenté dans la partie supérieure du nœud et dont le nom est entouré de flèches doubles.

### Diagramme des cas d'utilisation

Le  **diagramme de cas d'utilisation**  (**DCU**) est un diagramme UML  utilisé pour donner une vision globale du comportement fonctionnel d'un système  logiciel.
Il permet d'identifier les possibilités d'interaction entre le système et les acteurs (intervenants extérieurs au système), c'est-à-dire toutes les fonctionnalités que doit fournir le système. Il permet aussi de délimiter celui-ci. Un cas d'utilisation représente une d'interaction entre un utilisateur (humain ou machine) et un système. Dans un diagramme de cas d'utilisation, les utilisateurs sont appelés acteurs, ils interagissent avec les cas d'utilisation (use cases).

| Exemple de diagramme de cas d'utilisation | ![use_cases](img/use_cases.png)|
|--|--|

Crédit : https://laurent-audibert.developpez.com/Cours-UML/?page=diagramme-cas-utilisation

### Diagramme états-transitions

Le diagramme de séquence permet de représenter les différents états que peut prendre un objet ou un composant du système. Il donne une vue complète du comportement de l'objet ou du composant.
Il se compose de plusieurs éléments :
- un état initial (cercle noir)
- les états intermédiaires de l'objet (rectangles)
- les différentes transitions entre états (flèches)
- un état final (cercle noir entouré)

Les annotations au dessus des flèches de transition indiquent les évenements qui permettent de passer d'un état à l'autre.

| Exemple d'un diagramme d'états transition | ![diagramme séquence](img/diagrammeEtatsTransitions.png) |
|--|--|

### Diagramme de séquence

#### Objectif
Sert à représenter les communications avec et au sein du logiciel en développement informatique
● Représentation temporelle des interactions entre les objets
● Chronologie des messages échangés entre les objets et avec les acteurs

| Exemple de diagramme de séquence | ![Exemple de diagramme de séquence](img/diad_seq.png) |
|--|--|

#### Ligne de vie
Pour représente un acteur ou un objet on utilise une ligne de vie. Celle-ci représente un participant à une interaction.

| Exemple de ligne de vie | ![Ligne de vie](img/LigneVieSeq.png) |
|--|--|

#### Interactions
Il est également possible de faire interagir les acteur et objets via des messages. Ils sont représentés par des flèches. Ils sont présentés du haut vers le bas le long des lignes de vie, dans un ordre chronologique.

| Exemple de message entre acteurs | ![Message_seq](img/Message_seq.png) |
|--|--|

Pour les cas plus complexes, on peut intégrer des algorithmes dans les diagrammes de séquences. Ce sont des fragments. Par le biais de cadres d'interaction, on peut préciser les opérantes d'un ensemble de messages :

-   **alt** : fragments multiple alternatifs (si alors sinon)
-   **opt** : fragment optionnel
-   **par** : fragment parallèle (traitements concurrents)
-   **loop** : le fragment s'exécute plusieurs fois
-   **region** : région critique (un seul thread à la fois)
-   **neg** : une interaction non valable
-   **break** : représente des scenario d'exception
-   **ref** : référence à une interaction dans un autre diagramme
-   **sd** : fragment du diagramme de séquence en entier

#### Crédits
- [https://laurent-audibert.developpez.com/Cours-UML/?page=diagrammes-interaction](https://laurent-audibert.developpez.com/Cours-UML/?page=diagrammes-interaction)
- [https://lipn.univ-paris13.fr/~gerard/uml-s2/uml-cours05.html](https://lipn.univ-paris13.fr/~gerard/uml-s2/uml-cours05.html)
- [https://fr.wikipedia.org/wiki/Diagramme_de_séquence](https://fr.wikipedia.org/wiki/Diagramme_de_s%C3%A9quence)