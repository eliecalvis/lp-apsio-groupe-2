# Intro
La représentation via un schéma des interactions entre les utilisateurs et la machine permet d'organiser les fonctions qui seront proposées par l'interface, facilitant ainsi la phase de conception.

Il n'existe pas de diagramme "officiel" dans le langage UML (Unified Modeling Langage) propre à la modélisation des IHM. C'est pourquoi les SNI (Systèmes de Navigation des Interface) permet de pallier à ce soucis.
----

# **Outils et langages de description d’écrans**

## **Les modèles**

#### **LA MODELISATION DYNAMIQUE SOUS UML**

Un comportement = Une réaction. Les diagrammes regroupés dans cette catégorie permettent de modéliser le comportement dynamique du système.
On s'intéresse particulièrement à trois d’entre eux :


- ***Diagramme d'activité***

Le diagramme d’activités fournit une représentation du comportement d’un système en décomposant, de façon fonctionnelle, les traitements et plus particulièrement, les cheminement de flots de contrôle et de flots de données. Ils sont particulièrement adaptés à la description des cas d'utilisation. (Le + : définition assez précise pour pouvoir générer du code)


- ***Diagramme d’état-transition***

Le diagramme d’état-transition, qui représente les états possibles des objets d’une classe donnée ainsi que les transitions entre ses états. Il est basé sur les concepts d’état (situation observée à un moment déterminé), transition (passage d’un état à l’autre), évènement (ensemble d’informations pouvant déclencher la transition) et d’activité (action ayant une durée dans le temps). (Le +:définition assez précise pour pouvoir générer du code)

- ***Diagramme d’interactions***

Le diagramme d’interactions offre une vue sur l’ensemble des flux de contrôle du système. Il est réalisé avec le même graphisme que le diagramme d’activité. Chaque élément du diagramme peut ensuite être détaillé à l’aide d’un diagramme de séquence ou d’un diagramme d’activité.


**Le SNI (Schema Navigationnel d'interaction)**

Le SNI est purement conceptuel. Il est indépendant de la technologie utilisée, du type d'IHM (menu, bouton, toucher) et de tout aspect matériel (clavier, type d'écran, souris, etc.) Il permet une représentation simple et claire de l'enchainement des écrans d'une IHM et peut ainsi etre compris par tous les intervenants du projet.
Conclusion : Ce modèle intègre des éléments facilitant la représentation visuelle des éléments d'interaction présents dans l'interface. Il facilite, dès le début des étapes de conception, la visualisation des classes nécessaires au développement.


**Le modèles des tâches**

Tâche = Activité. Les modèles de tâches sont des descriptions logiques des activités à réaliser pour atteindre les objectifs des utilisateurs. Ils sont particulièrement utiles à la conception, l’analyse et l’évaluation les applications logicielles interactives. Une tâche est composée d’un but (état à atteindre), d’un dispositif (méthodes, outils, etc. nécessaires à l’atteinte du but), des actions (tâches simples), de tâches (activités nécessaires) et des sous-tâches. Conclusion : l’objectif de ce modèle est de comprendre et évaluer l’utilisabilité d’un système interactif. Pour atteindre son objectif, le modèle peut faire appel à différentes méthodes, dont la “Méthode analytique de description de tâches” (MAD), qui est à l’origine d’outils ciblés (K-MADe, voir ci-dessous) dans le domaine de la modélisation des interactions homme-machine.

----------

## Les logiciels

MICROSOFT VISIO
---------------
---

* Permet la création de diagrammes de façon simple et intuitive. Microsoft
* Visio permet également la collaboration et la sérisation des données.

__Avantages__
* C'est un logiciel complet qui offre rendu de qualité

__Inconvénients__
* Payant et propriétaire, il ne fonctionne que sur MSWindows.

DRAW.IO
----------
---

* Draw.io est un outil de création de diagrammes en ligne.

__Avantages__
* Il est gratuit et offre un large panel de diagrammes.

__Inconvénients__
* Il ne génère pas de code.


VISUALSNI
---------
---

* VisualSNI est un éditeur graphique spécifique de SNI sous la forme d'un plugin Eclipse.

__Avantages__
* Permet la génération de code.

__Inconvénients__
* Disponible seulement sous Eclipse

## Dessin / Génération de code
Gliffy : [Cliquez-moi](https://www.gliffy.com/)
------------------------------------------------
Gliffy Diagrams est un tout nouveau genre d'application Chrome qui FONCTIONNE MÊME HORS LIGNE !
Désormais, n'importe qui peut créer des diagrammes et des organigrammes de qualité professionnelle rapidement et simplement.

Avantages :
- Supporté par tous les navigateurs actuels
- Pas besoin de télécharger
- Travail collaboratif
- Interface utilisateur rapide et clair
- Compatible avec UML-2
- Documentation clair

Inconvénients :
- Certaines fonctionnalités comme l'export d'images et l'interface Google Drive sont payantes
- Pas de debugger

![Gliffy](img/image-diagram-flowchart.png)

DIA : [Cliquez-moi](http://dia-installer.de/doc/index.html)
------------------------------------------------
Dia est un programme pour faire des diagrammes structurés.
![DIA](img/dia_screenshot.png)

K-MADe : [Cliquez-moi](http://www.gt-mdt.fr/fr/la-boite-a-outils-du-gt/le-portail-des-notations/k-mad-et-son-outil-k-made)
------------------------------------------------
Il est principalement destiné, quoique pas exclusivement, aux ergonomes et aux spécialistes de l'interaction homme machine (IHM). K-MADe permet de créer, modifier, interroger, et simuler des modèles de tâches. Issu d'une collaboration entre ergonomie et chercheurs en IHM, il a pour but de faciliter la mise en œuvre d'une approche centrée-utilisateur, basée sur modèles, pour la conception et l'évaluation ergonomique des applications interactives.

![K-MADe](img/task-model.jpg)
----------

## Tableau récapitulatif

||Licence Proposée|Spécifique aux IHM|Génération de Code|
|--- |--- |--- |--- |
|Dia|GPL|Non|Non|
|Draw|Gratuit|Non|-|
|Microsoft Visio|Payant|Non|Non|
|Visual SNI|GPL|Oui|Oui|
|COMM (eCOMM)|Gratuit|Oui|-|
|K-MAD (K-MADe)|GPL|Oui|-|
|HAMSTERS|Gratuit|Oui|-|
