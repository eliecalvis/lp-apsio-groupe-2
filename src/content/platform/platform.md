﻿# Plateformes de développement

## Introduction

Un **IDE** ou Environnement de développement (Integrated Development Environment) est un logiciel qui rassemble des **outils** permettant de **développer** d'autres logiciels tels que des **applications mobiles, des logiciels pour ordinateur ou consoles de jeux, des sites web, etc...** ainsi que de réaliser des librairies ou des Framework.

 **Quel environnement choisir pour développer une application Android ?**

## Les différents IDE

### Android Studio

**Android Studio est l'environnement de développement officiel pour développer des applications Android.** Annoncé en mai 2013, la version 1.0 sort le 8 décembre 2014 et remplace Eclipse au titre d'IDE conseillé par Google.

![Logo Android Studio](https://s1.qwant.com/thumbr/700x0/a/a/ee69991b004cb01c9e048adbc0203c4c06b2c33db7ca7d2aafea37e3824bbb/android-studio-logo-840x359.png?u=https://cdn57.androidauthority.net/wp-content/uploads/2017/05/android-studio-logo-840x359.png&q=0&b=1&p=0&a=1)

- **Langages :**

**Java** est le langage le plus utilisé dans le développement d'applications Android, langage officiellement recommandé par Google. Il laisse cependant depuis 2017 progressivement la place à **Kotlin**  qui est, depuis le 8 mai 2019, le langage recommandé par Google pour le développement des applications Android.

Le langage à balise **XML** permet d'optimiser le développement en gérant l'affichage des contenus sur l'écran.
Android Studio offre des ressources qui facilite l'apprentissage de ces trois langages et permet ainsi à n'importe quel utilisateur de réaliser ses projets efficacement.

Android Studio permet aussi l'utilisation des langages **C/C++**, **C#**, **BASIC**,  et **Corona/LUA**

- **Interface :**

![Interface Android Studio](https://s1.qwant.com/thumbr/0x0/0/6/a405388cea449e3c4dda0cac76251ff11c96431523343378cadcc08e7a35db/android-studio-screen.jpg?u=https://www.softwaretestingmagazine.com/wp-content/uploads/android-studio-screen.jpg&q=0&b=1&p=0&a=1)

Android Studio permet d'afficher l'aperçu du projet en cours sur un appareil physique ou d'émuler différents appareils compatible afin d'obtenir un aperçu sur différents modèles.

### Netbeans
![enter image description here](https://cwiki.apache.org/confluence/download/attachments/67635710/Logo-NetBeans-160401-03.jpg?version=1&modificationDate=1482352437000&api=v2)

Netbeans **est un environnement de développement intégré** (EDI) placé open-source par l'entreprise Sun Microsystems en 2000. En 2016 l'Apache Software Foundation reprends le développement de Netbeans sous **licence CDDL (Common Development and Distribution License) et GPLv2.**

  - **Langages:**
  
  Netbeans a été conçu en Java et permet de supporter des langages de programmation comme le Java, Python, C, C++, JavaScript, XML, Ruby, PHP et HTML. Il comprend toutes les caractéristiques d'un IDE moderne (éditeur en couleur, projets multi-langage, refactoring, éditeur graphique d'interfaces et de pages Web).

  - **Multi-plateforme:**
    
  NetBeans est disponible sous Windows, Linux, Solaris (sur x86 et architecture SPARC), Mac OS X ou sous une version indépendante des systèmes d'exploitation (requérant une machine virtuelle Java). Un environnement Java Development Kit JDK est requis pour les développements en Java.

### Visual Studio Code
![enter image description here](https://blog.launchdarkly.com/wp-content/uploads/2018/10/visualstudio_code-card.png)

Visual Code est un éditeur de code open-source sous **licence MIT** développé par Microsoft. Il propose différents éléments qui peuvent être intéressants pour des développeurs de tous niveau. VSC est développé avec **Electron** (framework permettant de développer des applications multi-plateforme de bureau).

  - **Langages:**
  
VSC supporte une dizaine de langage de programmation, certains déjà inclut dans  VSC, mais il est possible de trouver des extensions de langage dans le VSC Marketplace.
Parmi eux, les plus populaire sont Python, C/C++, C#, Java, PHP ou encore Ruby.

  - **Multi-plateforme:**
    
  VSC est disponible sous Windows, Linux et macOS.