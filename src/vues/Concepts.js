import React from 'react'
import concepts from '../content/concepts/concepts.md'
import MDContent from './MDContent'
const ReactMarkdown = require('react-markdown')



export default class Concepts extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      md: null,
    }
  }

  componentWillMount() {
    fetch(concepts).then((resp) => {
      resp.text().then((text) => this.setState({md: text}) )
    })
  }

  render() {
    return <MDContent title="Conception UML">
        <ReactMarkdown source={this.state.md} />
      </MDContent>
  }
}
