import React from 'react'
import tools from '../content/tools/tools.md'
import MDContent from './MDContent'
const ReactMarkdown = require('react-markdown')



export default class Tools extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      md: null,
    }
  }

  componentWillMount() {
    fetch(tools).then((resp) => {
      resp.text().then((text) => this.setState({md: text}) )
    })
  }

  render() {
    return <MDContent title="Outils">
        <ReactMarkdown source={this.state.md} />
      </MDContent>
  }
}
