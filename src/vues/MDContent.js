import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Footer from '../components/Footer'
import Header from '../components/Header';

export default function MDContent(props) {

  return (
    <React.Fragment>
      <CssBaseline />
      <Header />
      <main>
      <Container maxWidth="lg">
            <Typography component="p" align="center" color="textPrimary" gutterBottom>
            {props.children}
            </Typography>
      </Container>
      </main>
      <Footer />
    </React.Fragment>
  );
}