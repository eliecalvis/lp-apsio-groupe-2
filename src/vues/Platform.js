import React from 'react'
import platform from '../content/platform/platform.md'
import MDContent from './MDContent'
const ReactMarkdown = require('react-markdown')



export default class Platform extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      md: null,
    }
  }

  componentWillMount() {
    fetch(platform).then((resp) => {
      resp.text().then((text) => this.setState({md: text}) )
    })
  }

  render() {
    return <MDContent title="Plateforme Android">
        <ReactMarkdown source={this.state.md} />
      </MDContent>
  }
}
