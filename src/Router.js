import React from 'react'
import { BrowserRouter as Router, Route} from "react-router-dom";
import Accueil from './vues/Accueil';
import Platform from './vues/Platform';
import Concepts from './vues/Concepts';
import Tools from './vues/Tools';

const WebRouter = () => {
  return <Router basename={process.env.PUBLIC_URL}>
      <Route exact path="/" component={Accueil} />
      <Route exact path="/tools" component={Tools}/>
      <Route exact path="/concepts" component={Concepts}/>
      <Route exact path="/platform" component={Platform} />
  </Router>
}

export default WebRouter