import React from 'react';
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Button } from '@material-ui/core';


export default function Header(props){
  return (
      <AppBar position="relative">
        <Toolbar>
          <Typography variant="h6" color="inherit" noWrap>

          <Link to={'/'} style={{ textDecoration: 'none', color: 'black', fontWeight: 'bold' }} >
            <Button variant="contained" color="2ecc71">
                Accueil
            </Button>
          </Link>
          </Typography>
        </Toolbar>
      </AppBar>
  )
}